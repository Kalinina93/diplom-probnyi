# Открыть приложение
# Войти в админку
# Проверить, что пользователь создан в бд
# Выйти из приложения
# Войти как созданный пользователь
# Проверить, что приложение открылось

# Открыть приложение
# Войти в админку
# Удалить первое созданное изображение
# Вернуться на главную страницу
# Убедиться, что первое изображение не отображается на странице

import time

from Test_djangoproject.tests.pages.admin_page import AdminPage
from Test_djangoproject.tests.pages.login_page import LoginPage
from Test_djangoproject.tests.tests.test_base import BaseTest
from Test_djangoproject.tests.utils.config import TestDatas
from Test_djangoproject.tests.utils.locators import LoginPageLocators, GroupPageLocators, UserPageLocators, \
    PostPageLocators, AdminPageLocators


class TestLoginNewUser(BaseTest):

    def test_new_user_exist(self):
        """Checking that new user_2 is created"""
        self.loginPage = LoginPage(self.driver)
        self.loginPage.do_login(TestDatas.USERNAME, TestDatas.PASSWORD)
        self.adminPage = AdminPage(self.driver)
        self.adminPage.open_user_page()
        text = self.adminPage.get_element_text(UserPageLocators.IS_USER_EXIST)
        assert text == "3 users", f"Error"
        self.adminPage.do_click(AdminPageLocators.LOG_OUT)

    def test_new_user_login(self):
        """Testing login with new user_2"""
        self.loginPage = LoginPage(self.driver)
        self.loginPage.do_send_keys(LoginPageLocators.USERNAME_FIELD, "user_2")
        self.loginPage.do_send_keys(LoginPageLocators.PASSWORD_FIELD, "*VpH6-HG.cay3fE")
        self.loginPage.do_click(LoginPageLocators.LOG_IN_BUTTON)
        self.adminPage = AdminPage(self.driver)
        self.adminPage.open_admin_page()
        text = self.loginPage.get_element_text(LoginPageLocators.IS_ADMIN_PAGE)
        assert text == "Site administration", f"Error"


class TestDeleteImage(BaseTest):

    def test_post_page(self):
        """Checking location on window posts"""
        self.loginPage = LoginPage(self.driver)
        self.loginPage.do_login(TestDatas.USERNAME, TestDatas.PASSWORD)
        self.loginPage.open_admin_page()
        self.loginPage.do_click(PostPageLocators.POSTS)
        text = self.loginPage.get_element_text(PostPageLocators.IS_POSTS_PAGE)
        assert text == "Select post to change", f"Error"

    def test_post_page_title(self):
        """Testing correct title of post page"""
        # self.loginPage = LoginPage(self.driver)
        # self.loginPage.do_login(TestDatas.USERNAME, TestDatas.PASSWORD)
        self.loginPage.open_admin_page()
        self.loginPage.do_click(PostPageLocators.POSTS)
        title = self.loginPage.get_title(PostPageLocators.POSTS_TITLE)
        assert title == PostPageLocators.POSTS_TITLE, f"Error"

    def test_delete_image(self):
        """Delete the first post"""
        # self.loginPage = LoginPage(self.driver)
        # self.loginPage.do_login(TestDatas.USERNAME, TestDatas.PASSWORD)
        self.loginPage.open_admin_page()
        self.loginPage.do_click(PostPageLocators.POSTS)
        self.loginPage.do_click(PostPageLocators.POST_OBJECT_1)
        self.loginPage.do_click(PostPageLocators.ACTION)
        self.loginPage.do_send_keys(PostPageLocators.ACTION, "D")
        self.loginPage.do_click(PostPageLocators.DELETE_SELECT_POSTS)
        self.loginPage.do_click(PostPageLocators.GO)
        self.loginPage.do_click(PostPageLocators.YES_I_AM_SURE)
        self.loginPage.open_admin_page()
        self.loginPage.do_click(AdminPageLocators.POSTS)
        text = self.loginPage.get_element_text(PostPageLocators.POSTS_NUM)
        assert text == "3 posts", f"There are 4 posts"
        time.sleep(2)







