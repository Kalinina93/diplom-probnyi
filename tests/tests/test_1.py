# Открыть приложение
# Войти в админку
# Проверить, что группа отображается

# Открыть приложение
# Войти в админку
# Проверить, что в базе данных пользователь добавлен в группу


from selenium.webdriver.common.by import By

from Test_djangoproject.tests.pages.admin_page import AdminPage
from Test_djangoproject.tests.pages.login_page import LoginPage
from Test_djangoproject.tests.tests.test_base import BaseTest
from Test_djangoproject.tests.utils.config import TestDatas
from Test_djangoproject.tests.utils.locators import LoginPageLocators, GroupPageLocators, UserPageLocators


class TestLogin(BaseTest):

    def test_username_link_is_visible(self):
        """Testing username link for visible"""
        self.loginPage = LoginPage(self.driver)
        flag = self.loginPage.is_username_link_exist()
        assert flag, f"Username link is not visible"

    def test_password_link_is_visible(self):
        """Testing password link for visible"""
        self.loginPage = LoginPage(self.driver)
        flag = self.loginPage.is_password_link_exist()
        assert flag, f"Password link is not visible"

    def test_login_page_title(self):
        """Testing correct title of login page"""
        self.loginPage = LoginPage(self.driver)
        title = self.loginPage.get_title(LoginPageLocators.LOGIN_TITLE)
        assert title == LoginPageLocators.LOGIN_TITLE, f"Error"

    def test_login(self):
        """Checking the location of window administration"""
        self.loginPage = LoginPage(self.driver)
        self.loginPage.do_login(TestDatas.USERNAME, TestDatas.PASSWORD)
        self.loginPage.open_admin_page()
        text = self.loginPage.get_element_text(LoginPageLocators.IS_ADMIN_PAGE)
        assert text == "Site administration", f"Error"

    def test_group_exist(self):
        """Checking that creating group_1 is existed"""
        self.adminPage = AdminPage(self.driver)
        self.adminPage.open_group_page()
        text = self.adminPage.get_element_text(GroupPageLocators.GROUP_EXIST)
        assert text == "1 group", f"Error"


class TestAdmin(BaseTest):

    def test_group_page(self):
        """Checking of location window of groups """
        self.loginPage = LoginPage(self.driver)
        self.loginPage.do_login(TestDatas.USERNAME, TestDatas.PASSWORD)
        self.loginPage.open_admin_page()
        self.adminPage = AdminPage(self.driver)
        self.adminPage.open_group_page()
        text = self.adminPage.get_element_text(GroupPageLocators.IS_GROUP_PAGE)
        assert text == "Select group to change", f"Error"

    def test_group_page_title(self):
        """Testing correct group title"""
        # self.loginPage = LoginPage(self.driver)
        # self.loginPage.do_login(TestDatas.USERNAME, TestDatas.PASSWORD)
        self.adminPage = AdminPage(self.driver)
        self.adminPage.open_group_page()
        title = self.adminPage.get_title(GroupPageLocators.GROUP_TITLE)
        assert title == GroupPageLocators.GROUP_TITLE, f"Error"

    def test_group_search_link_is_visible(self):
        """Testing that group search link is visible"""
        # self.loginPage = LoginPage(self.driver)
        # self.loginPage.do_login(TestDatas.USERNAME, TestDatas.PASSWORD)
        self.adminPage = AdminPage(self.driver)
        self.adminPage.open_group_page()
        flag = self.adminPage.is_visible(GroupPageLocators.SEARCH_FIELD)
        assert flag, f"Username link is not visible"

    def test_user_page_title(self):
        """Testing correct title of user page"""
        # self.loginPage = LoginPage(self.driver)
        # self.loginPage.do_login(TestDatas.USERNAME, TestDatas.PASSWORD)
        self.adminPage = AdminPage(self.driver)
        self.adminPage.open_user_page()
        title = self.adminPage.get_title(UserPageLocators.USER_TITLE)
        assert title == UserPageLocators.USER_TITLE, f"Error"

    def test_user_search_is_visible(self):
        """Testing that user search link is visible"""
        # self.loginPage = LoginPage(self.driver)
        # self.loginPage.do_login(TestDatas.USERNAME, TestDatas.PASSWORD)
        self.adminPage = AdminPage(self.driver)
        self.adminPage.open_user_page()
        flag = self.adminPage.is_visible(UserPageLocators.SEARCH_FIELD)
        assert flag, f"Search field is not visible"

    def test_adding_user_in_group(self):
        """Checking that creating user_1 added in group_1"""
        # self.loginPage = LoginPage(self.driver)
        # self.loginPage.do_login(TestDatas.USERNAME, TestDatas.PASSWORD)
        self.adminPage = AdminPage(self.driver)
        self.adminPage.open_user_page()
        self.adminPage.do_send_keys(UserPageLocators.SEARCH_FIELD, "user_1")
        self.adminPage.do_click(UserPageLocators.SEARCH_BUTTON)
        self.adminPage.do_click(UserPageLocators.USER_1)
        text = self.adminPage.get_element_text(UserPageLocators.IS_ADDING_GROUP)
        assert text == "group_1", f"Error"
        







