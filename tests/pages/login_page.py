from Test_djangoproject.tests.pages.base_page import BasePage
from Test_djangoproject.tests.utils.config import TestDatas
from Test_djangoproject.tests.utils.locators import LoginPageLocators


class LoginPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self.driver.get(TestDatas.BASE_URL)

    def get_login_page_title(self, title):
        return self.get_title(title)

    def is_username_link_exist(self):
        return self.is_visible(LoginPageLocators.USERNAME_FIELD)

    def is_password_link_exist(self):
        return self.is_visible(LoginPageLocators.PASSWORD_FIELD)

    def do_login(self, username, password):
        self.do_send_keys(LoginPageLocators.USERNAME_FIELD, username)
        self.do_send_keys(LoginPageLocators.PASSWORD_FIELD, password)
        self.do_click(LoginPageLocators.LOG_IN_BUTTON)



