from Test_djangoproject.tests.pages.base_page import BasePage
from Test_djangoproject.tests.utils.config import TestDatas
from Test_djangoproject.tests.utils.locators import AdminPageLocators, UserPageLocators


class AdminPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self.driver.get(TestDatas.ADMIN_URL)

    def get_admin_page_title(self, title):
        return self.get_title(title)

    def open_group_page(self):
        self.do_click(AdminPageLocators.GROUPS)

    def open_user_page(self):
        self.do_click(AdminPageLocators.USERS)







