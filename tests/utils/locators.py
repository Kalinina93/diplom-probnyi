from selenium.webdriver.common.by import By


class LoginPageLocators:
    USERNAME_FIELD = (By.NAME, "username")
    PASSWORD_FIELD = (By.NAME, "password")
    LOG_IN_BUTTON = (By.CSS_SELECTOR, "#login-form > div.submit-row > input[type=submit]")
    IS_ADMIN_PAGE = (By.CSS_SELECTOR, "#content > h1")
    LOGIN_TITLE = "Log in | Django site admin"


class AdminPageLocators:
    APP = (By.CLASS_NAME, "section")
    POSTS = (By.CSS_SELECTOR, "#content-main > div.app-app.module > table > tbody > tr > th > a")
    AUTHENTICATION_AND_AUTHORIZATION = (By.CSS_SELECTOR, "#content-main > div.app-auth.module > table > caption > a")
    GROUPS = (By.CSS_SELECTOR, "#content-main > div.app-auth.module > table > tbody > tr.model-group > th > a")
    USERS = (By.CSS_SELECTOR, "#content-main > div.app-auth.module > table > tbody > tr.model-user > th > a")
    LOG_OUT = (By.CSS_SELECTOR, "#user-tools > a:nth-child(4)")


class GroupPageLocators:
    SEARCH_FIELD = (By.ID, "searchbar")
    SEARCH_BUTTON = (By.CSS_SELECTOR, "#changelist-search > div > input[type=submit]:nth-child(3)")
    GROUP_EXIST = (By.CLASS_NAME, "paginator")
    IS_GROUP_PAGE = (By.CSS_SELECTOR, "#content > h1")
    GROUP_TITLE = "Select group to change | Django site admin"


class UserPageLocators:
    SEARCH_FIELD = (By.NAME, "q")
    SEARCH_BUTTON = (By.CSS_SELECTOR, "#changelist-search > div > input[type=submit]:nth-child(3)")
    USER_EXIST = (By.CLASS_NAME, "paginator")
    USER_TITLE = "Select user to change | Django site admin"
    IS_ADDING_GROUP = (By.CSS_SELECTOR, "#id_groups_to > option")
    USER_1 = (By.CSS_SELECTOR, "#result_list > tbody > tr > th > a")
    IS_USER_EXIST = (By.CSS_SELECTOR, "#changelist-form > p")


class PostPageLocators:
    POSTS = (By.CSS_SELECTOR, "#content-main > div.app-app.module > table > tbody > tr > th > a")
    ACTION = (By.NAME, "action")
    GO = (By.CLASS_NAME, "button")
    DELETE_SELECT_POSTS = (By.CSS_SELECTOR, "#changelist-form > div.actions > label > select > option:nth-child(2)")
    POST_OBJECT_1 = (By.CSS_SELECTOR, "#result_list > tbody > tr:nth-child(4) > td > input")
    IS_POSTS = (By.CSS_SELECTOR, "#changelist-form > p")
    POSTS_TITLE = "Select post to change | Django site admin"
    IS_POSTS_PAGE = (By.CSS_SELECTOR, "#content > h1")
    YES_I_AM_SURE = (By.CSS_SELECTOR, "#content > form > div > input[type=submit]:nth-child(4)")
    POSTS_NUM = (By.CSS_SELECTOR, "#changelist-form > p")


